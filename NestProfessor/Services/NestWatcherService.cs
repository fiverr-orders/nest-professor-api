using System.Threading;
using NestBridge;
using NestProfessor.Models;

namespace NestProfessor.Services
{
    public class NestWatcherService
    {
        private readonly NestProcessorEx _nestProcessor;
        private readonly NestResult _nestResult;
        private readonly int _startTickCount;
        private readonly int _nestingTime;
        
        public NestWatcherService(NestProcessorEx nestProcessor, NestResult nestResult, int nestingTime)
        {
            _nestProcessor = nestProcessor;
            _nestResult = nestResult;
            _nestingTime = nestingTime;
            _startTickCount = System.Environment.TickCount;
        }

        public void Run()
        {
            while (true)
            {
                Thread.Sleep(500);
                
                if (_nestProcessor.BetterResultExist())
                {
                    var sheetList = _nestProcessor.GetNestResult();
                    _nestResult.AddNestResult(sheetList);
                    _nestProcessor.WriteLog("the watcher thread got a result from the kernel.");
                }
                
                if (_nestProcessor.IsStopped() && !_nestProcessor.BetterResultExist())
                {
                    _nestProcessor.WriteLog("the watcher thread quited.");
                    _nestResult.TaskFinished(true);
                    break;
                }
                
                var elapsedTime = (System.Environment.TickCount - _startTickCount) / 1000;
                _nestResult.SetElapsedTime(elapsedTime);
                
                if (elapsedTime > _nestingTime)
                {
                    _nestProcessor.StopNest();
                    _nestProcessor.WriteLog("the watcher thread sent the stop command to the kernel.");
                }
            }
        }
    }
}