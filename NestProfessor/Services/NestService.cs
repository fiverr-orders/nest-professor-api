using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using NestBridge;
using NestProfessor.Models;

namespace NestProfessor.Services
{
    public class NestService
    {
        private NestTaskEx _nestTaskEx;
        private System.Timers.Timer _timer;
        private NestResult _nestResult;
        private SheetListEx _sheetList;
        private int _nestResultCount = 0;

        public NestResponse StartNest(Nest nest)
        {
            var nestingTime = nest.Time;
            var nestParams = new NestParamEx();
            var nestParts = BuildNestPartList(nest.Parts);

            if (nestParts.Size() == 0)
            {
                throw new InvalidOperationException("No part will be nested.");
            }

            var materials = BuildMaterials(nest.Sheets);

            if (materials.Size() == 0)
            {
                throw new InvalidOperationException("No material will be used for nesting.");
            }

            _nestTaskEx = new NestTaskEx(nestParts, materials, nestParams);
            var nestProcessor = NestFacadeEx.StartNest(this._nestTaskEx);

            if (nestProcessor == null)
            {
                throw new InvalidOperationException(
                    "Cannot start the nesting task, please contact TAOSoft and check your license."
                );
            }

            var result = GetNestResult(nestProcessor, nestingTime);

            return result;
        }

        private GeomItemListEx BuildGeoItemsList(IList<double[]> geometry)
        {
            var geomItemList = new GeomItemListEx();
            for (var i = 0; i < geometry.Count; i++)
            {
                var currentPoint = geometry[i];
                var nextPoint = i == geometry.Count - 1 ? geometry[0] : geometry[i + 1];

                var startPoint = new Point2DEx(currentPoint[0], currentPoint[1]);
                var endPoint = new Point2DEx(nextPoint[0], nextPoint[1]);

                var lineItemEx = new LineItemEx(startPoint, endPoint);

                geomItemList.AddItem(lineItemEx);
            }

            return geomItemList;
        }

        private PART_ROT_STYLE_EX GetRotateAngle(string angle)
        {
            return angle switch
            {
                "Free Rotate" => PART_ROT_STYLE_EX.PART_ROT_FREE,
                "90 Increment" => PART_ROT_STYLE_EX.PART_ROT_PID2_INCREMENT,
                "180 Increment" => PART_ROT_STYLE_EX.PART_ROT_PI_INCREMENT,
                "0 Fixed" => PART_ROT_STYLE_EX.PART_ROT_ZERO_FIXED,
                "90 Fixed" => PART_ROT_STYLE_EX.PART_ROT_PID2_FIXED,
                "180 Fixed" => PART_ROT_STYLE_EX.PART_ROT_PI_FIXED,
                "270 Fixed" => PART_ROT_STYLE_EX.PART_ROT_PID23_FIXED,
                "Custom Rotation" => PART_ROT_STYLE_EX.PART_ROT_CUSTOM_ANG,
                _ => throw new InvalidDataException(
                    $"{angle} is not an underlying value of the PART_ROT_STYLE_EX enumeration."
                )
            };
        }

        private NestPartEx BuildNestPart(GeomItemListEx geomItemList, NestParamEx nestParams,
            PartInstance instance)
        {
            var partEx = new PartEx(string.Empty, geomItemList, new AncillaryDataEx());
            var isClosedBoundary = NestFacadeEx.HasClosedBoundary(partEx, nestParams.GetConTol());
            if (!isClosedBoundary)
            {
                throw new InvalidDataException("The part which do not has a closed boundary cannot be nested.");
            }

            var angle = GetRotateAngle(instance.Orientation.Angle);

            var nestPart = new NestPartEx(
                partEx, NestPriorityEx.MaxPriority(), instance.Quantity, angle, instance.Orientation.Flip
            );

            return nestPart;
        }

        private NestPartListEx BuildNestPartList(IEnumerable<Part> parts)
        {
            var nestParams = new NestParamEx();
            var nestPartList = new NestPartListEx();

            foreach (var part in parts)
            {
                var geomItemList = BuildGeoItemsList(part.Geometry);

                foreach (var instance in part.Instances)
                {
                    var nestPart = BuildNestPart(geomItemList, nestParams, instance);
                    nestPartList.AddNestPart(nestPart);
                }
            }

            return nestPartList;
        }

        private MatListEx BuildMaterials(IEnumerable<Sheet> sheets)
        {
            var materials = new MatListEx();
            foreach (var sheet in sheets)
            {
                var rect = new Rect2DEx(0, sheet.Width, 0, sheet.Height);
                var rectMat = new RectMatEx("New Material", rect, sheet.Quantity);

                materials.AddMat(rectMat);
            }

            return materials;
        }

        private NestResponse GetNestResult(NestProcessorEx nestProcessor, int nestingTime)
        {
            InitTimer();
            _nestResult = new NestResult();
            var nestResultService = new NestWatcherService(nestProcessor, _nestResult, nestingTime);
            var thread = new Thread(nestResultService.Run);
            
            thread.Start();
            thread.Join();
            
            return BuildNestResult();
        }

        private void InitTimer()
        {
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += TimeElapsed_Tick;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }
        
        private void TimeElapsed_Tick(object source, System.Timers.ElapsedEventArgs e) 
        {
            var finished = _nestResult.TaskFinished() && _nestResultCount == _nestResult.GetNestResultCount();

            if (finished)
            {
                _timer.Enabled = false;
            }
            else
            {
                if (_nestResultCount >= _nestResult.GetNestResultCount()) 
                    return;
                
                _nestResultCount++;
                _sheetList = _nestResult.GetNestResultByIndex(_nestResultCount - 1);
            }
        }

        private NestResponse BuildNestResult()
        {
            if (_sheetList == null)
                return null;

            var sheets = BuildSheetResult();
            var parts = BuildPartNestResult();
            var materials = BuildMaterialResults();

            var result = new NestResponse()
            {
                Parts = parts,
                Sheets = sheets,
                Materials = materials
            };

            return result;
        }

        private List<MaterialResult> BuildMaterialResults()
        {
            var matList = _nestTaskEx.GetMatList();
            var materials = new List<MaterialResult>();

            for (var i = 0; i < matList.Size(); i++)
            {
                var material = matList.GetMatByIndex(i);
                var count = i + 1;
                var consumedCount = _sheetList.GetSheetCount(material.GetID());

                materials.Add(new MaterialResult()
                {
                    Num = count.ToString(),
                    MaterialName = material.GetName(),
                    SubmittedCount = material.GetCount(),
                    ConsumedCount = consumedCount
                });
            }

            return materials;
        }

        private IList<PartNestResult> BuildPartNestResult()
        {
            var nestPartList = this._nestTaskEx.GetNestPartList();
            var parts = new List<PartNestResult>();

            for (var i = 0; i < nestPartList.Size(); i++)
            {
                var nestPart = nestPartList.GetNestPartByIndex(i);
                var part = nestPart.GetPart();
                var count = i + 1;
                var nestedCount = _sheetList.GetPartInstCount(part.GetID());

                parts.Add(new PartNestResult()
                {
                    Num = count.ToString(),
                    PartName = part.GetName(),
                    SubmittedCount = nestPart.GetNestCount(),
                    NestedCount = nestedCount
                });
            }

            return parts;
        }

        private IList<SheetResult> BuildSheetResult()
        {
            var sheets = new List<SheetResult>();
            for (var i = 0; i < _sheetList.Size(); i++)
            {
                var sheet = _sheetList.GetSheetByIndex(i);
                sheets.Add(new SheetResult()
                {
                    Num = i + 1,
                    SheetName = sheet.GetName(),
                    Count = sheet.GetCount(),
                    MaterialName = sheet.GetMat().GetName(),
                    Id = sheet.GetID()
                });
            }

            return sheets;
        }
    }
}