namespace NestProfessor.Models
{
    public class SheetResult
    {
        public int Num { get; set; }
        public string SheetName { get; set; }
        public int Count { get; set; }
        public string MaterialName { get; set; }
        public long Id { get; set; }
    }
}