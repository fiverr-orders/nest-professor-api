namespace NestProfessor.Models
{
    public class PartNestResult
    {
        public string Num { get; set; }
        public string PartName { get; set; }
        public int SubmittedCount { get; set; }
        public int NestedCount { get; set; }
    }
}