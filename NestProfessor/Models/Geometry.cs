using System.Collections.Generic;

namespace NestProfessor.Models
{
    public class Geometry
    {
        public IList<Point> Points { get; set; }

        public Geometry()
        {
            this.Points = new List<Point>();
        }
    }
}