using System.Collections.Generic;

namespace NestProfessor.Models
{
    public class PartInstance
    {
        public Orientation Orientation { get; set; }
        public int Quantity { get; set; }
        public long Id { get; set; }

        public PartInstance()
        {
            this.Orientation = new Orientation();
        }
    }
}