namespace NestProfessor.Models
{
    public class Orientation
    {
        public double MinAngle { get; set; }
        public double MaxAngle { get; set; }
        public bool Flip { get; set; }
        public string Angle { get; set; }
    }
}