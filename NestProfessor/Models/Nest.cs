using System.Collections.Generic;

namespace NestProfessor.Models
{
    public class Nest
    {
        public IList<Part> Parts { get; set; }
        public IList<Sheet> Sheets { get; set; }
        public int Time { get; set; }

        public Nest()
        {
            this.Parts = new List<Part>();
            this.Sheets = new List<Sheet>();
        }
    }
}