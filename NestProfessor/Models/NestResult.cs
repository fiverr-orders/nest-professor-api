using System.Collections;
using System.Threading;
using NestBridge;

namespace NestProfessor.Models
{
    public class NestResult
    {
        private readonly ArrayList _nestResults = new ArrayList();
        private readonly Mutex _mutex = new Mutex();
        private bool _taskFinished;
        private int _elapsedTime;

        public NestResult()
        {
        }

        public SheetListEx GetNestResultByIndex(int iIndex)
        {
            SheetListEx nestResult;

            _mutex.WaitOne();
            nestResult = (SheetListEx)_nestResults[iIndex];
            _mutex.ReleaseMutex();

            return nestResult;
        }

        public void AddNestResult(SheetListEx nestResult)
        {
            _mutex.WaitOne();
            _nestResults.Add(nestResult);
            _mutex.ReleaseMutex();
        }

        public int GetNestResultCount()
        {
            var count = 0;

            _mutex.WaitOne();
            count = _nestResults.Count;
            _mutex.ReleaseMutex();

            return count;
        }

        public int GetElapsedTime()
        {
            var elapsedTime = 0;

            _mutex.WaitOne();
            elapsedTime = _elapsedTime;
            _mutex.ReleaseMutex();

            return elapsedTime;
        }

        public void SetElapsedTime(int elapsedTime)
        {
            _mutex.WaitOne();
            _elapsedTime = elapsedTime;
            _mutex.ReleaseMutex();
        }

        public bool TaskFinished()
        {
            bool b;

            _mutex.WaitOne();
            b = _taskFinished;
            _mutex.ReleaseMutex();

            return b;
        }

        public void TaskFinished(bool taskFinished)
        {
            _mutex.WaitOne();
            _taskFinished = taskFinished;
            _mutex.ReleaseMutex();
        }
    }
}