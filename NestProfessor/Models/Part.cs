using System.Collections.Generic;

namespace NestProfessor.Models
{
    public class Part
    {
        public IList<double[]> Geometry { get; set; }
        public IList<PartInstance> Instances { get; set; }
        public decimal ProtectionOffset { get; set; }

        public Part()
        {
            this.Geometry = new List<double[]>();
            this.Instances = new List<PartInstance>();
        }
    }
}