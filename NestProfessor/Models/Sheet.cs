namespace NestProfessor.Models
{
    public class Sheet
    {
        public double Height { get; set; }
        public double Width { get; set; }
        public long Id { get; set; }
        public double BorderGap { get; set; }
        public int Quantity { get; set; }
    }
}