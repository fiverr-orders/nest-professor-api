namespace NestProfessor.Models
{
    public class MaterialResult
    {
        public string Num { get; set; }
        public string MaterialName { get; set; }
        public int SubmittedCount { get; set; }
        public int ConsumedCount { get; set; }
    }
}