using System.Collections.Generic;

namespace NestProfessor.Models
{
    public class NestResponse
    {
        public IList<PartNestResult> Parts { get; set; }
        public IList<SheetResult> Sheets { get; set; }
        public IList<MaterialResult> Materials { get; set; }
    }
}