using Microsoft.AspNetCore.Mvc;
using NestProfessor.Models;
using NestProfessor.Services;

namespace NestProfessor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NestController : ControllerBase
    {
        private readonly NestService _nestService;

        public NestController()
        {
            this._nestService = new NestService();
        }

        [HttpPost()]
        public NestResponse Post([FromBody] Nest nest)
        {
            return this._nestService.StartNest(nest);
        }
    }
}